using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMouvement : MonoBehaviour
{
    private Animator animator;
    private float inputX;
    
    public float speed;
    public float JumpHight;
    private Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        
    }

    // Update is called once per frame
    void Update()
    {
        inputX = Input.GetAxis("Horizontal");
  
        animator.SetBool("IsRunnig", false);
        animator.SetBool("IsJumping", false);
        animator.SetFloat("stop", -1);
        if (inputX != 0)
        {
            Vector2 Runforce = new Vector2(inputX * Time.deltaTime * speed, 0);
            rb.AddForce(Runforce);
            animator.SetBool("IsRunnig", true);
        }
        if (Input.GetButtonDown("Jump")){
            //Debug.Log("here");
            animator.SetBool("IsRunnig", false);
            animator.SetBool("IsJumping", true);

            //    rb.AddForce(new Vector2(0, Time.deltaTime * JumpHight));
            transform.position=new Vector2(transform.position.x, transform.position.y + JumpHight);

        }

        
    }
}
