using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class PackmanMovement : MonoBehaviour
{
    public float PackManspeed;
    private float Horizontal_Movement;
    private float Vertical_Movement;
    private Rigidbody rb;
    private int Score;
    private int nbCoin;
    // Start is called before the first frame update
    void Start()
    {
        

        
        rb = GetComponent<Rigidbody>();
        Score = 0;
        
    }

    // Update is called once per frame
    void Update()
    {
        Horizontal_Movement = Input.GetAxis("Horizontal");
        Vertical_Movement = Input.GetAxis("Vertical");
        if(Horizontal_Movement != 0)
        {
            rb.AddForce(new Vector3(Horizontal_Movement*PackManspeed*Time.deltaTime, 0,0));
        }
        if(Vertical_Movement != 0)
        {
            rb.AddForce(new Vector3(0,0,Vertical_Movement*PackManspeed*Time.deltaTime));
        }
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Coin")
        {
            //Destroy(collision.gameObject);
            collision.gameObject.SetActive(false);
            Score += 5;
            Debug.Log("new Score = " + Score);
        }
    }
}
