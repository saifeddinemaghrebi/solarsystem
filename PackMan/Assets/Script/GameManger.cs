using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class GameManger : MonoBehaviour
{

    private Vector3 coinPosition;
    public int chance = 2;
    private GameObject[] tubeEmpties;
    private GameObject[] listCoin;
    int nb ;
    // Start is called before the first frame update
    void Start()
    {

        tubeEmpties = GameObject.FindGameObjectsWithTag("EmptyTube");

        for (int i = 0; i < tubeEmpties.Length; i++)
        {
            int random = Random.Range(0, chance);
            //if you use int's with Random.Range, the max will be exclusive (exclusive means it will not include the highest number.  There are a lot of reasons to do this but thats not important right now)  so you need to subtract one from it to get the actual number which i've already done for you.
            if (random == chance - 1)
            {
                coinPosition = tubeEmpties[i].transform.position;

                SpawnCoin();
            }
        }

        listCoin = GameObject.FindGameObjectsWithTag("Coin");






    }

    // Update is called once per frame
    void Update()
    {
        foreach (GameObject coin in listCoin)
        {
            coin.transform.Rotate(Vector3.right);
        }
        
    }
    public void SpawnCoin()
    {
        GameObject obstacle = Resources.Load("Coin") as GameObject;
        Instantiate(obstacle, coinPosition, Quaternion.identity);
    }
}
