using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //variables
    [SerializeField] private float Runspeed;
    [SerializeField] private float WalkSpeed;
    [SerializeField] private float moveSpeed;
    [SerializeField] private float JumpHeight;
    private Vector3 moveDirection;
    private Vector3 velocity;
    [SerializeField] private bool isGrounded;
    [SerializeField] private float groundChekDistance;
    [SerializeField] private LayerMask groundMask;
    [SerializeField] private float gravity;
    //REFERENCES
    private CharacterController controller;
    private Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
        animator = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }
    private void Move()
    {
        isGrounded = Physics.CheckSphere(transform.position, groundChekDistance,groundMask);
        if (isGrounded && velocity.y < 0)
        {
            velocity.y =-2f;
        }
        float moveZ = Input.GetAxis("Vertical");
        float moveX = Input.GetAxis("Horizontal");
        moveDirection = new Vector3(0, 0, moveZ);
        moveDirection = transform.TransformDirection(moveDirection);
        if (isGrounded)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Jump();
            }
            if (moveDirection != Vector3.zero && !Input.GetKey(KeyCode.LeftShift))
            {

                //Walk
                Walk(moveZ);

            }
            else if (moveDirection != Vector3.zero && Input.GetKey(KeyCode.LeftShift))
            {

                //run
                Run(moveZ);

            }
            else if (moveDirection == Vector3.zero)
            {
                //Idle
                Idle();
                if (! Input.GetKey(KeyCode.X))
                {
                    if (moveX > 0)
                    {
                        animator.SetFloat("Blend", 8f);
                    }
                    if (moveX < 0)
                    {
                        animator.SetFloat("Blend", 7f);
                    }
                }
                else if (Input.GetKey(KeyCode.X))
                {
                    if (moveX > 0)
                    {
                        animator.SetFloat("Blend", 13f);
                    }
                    if (moveX < 0)
                    {
                        animator.SetFloat("Blend", 12f);
                    }
                }
            }
            moveDirection *= moveSpeed;
            
        }
      
        controller.Move(moveDirection*Time.deltaTime);
        velocity.y+= gravity * Time.deltaTime;
       if(velocity.y >= -2f)
        {
            if (velocity.y > -1.5f)
            {
                animator.SetFloat("Blend", 3f);
            }
            else if (velocity.y < -1.5f)
            {
                animator.SetFloat("Blend", 4f);
            }
        }
        controller.Move(velocity*Time.deltaTime);   
    }
private void Idle()
    {
        float x = animator.GetFloat("Blend");
        if (Input.GetKey(KeyCode.X))
        { animator.SetFloat("Blend", 9f); }
        else
        {
            if (x >= 0.5 && x < 2)
                animator.SetFloat("Blend", 0f, 0.1f, Time.deltaTime);
            else
                animator.SetFloat("Blend", 0f);
            /*Debug.Log(x);*/
        }
    }
    private void Walk(float Movez)
    {

        moveSpeed = WalkSpeed;
        if (Movez > 0f)
        {
            if (Input.GetKey(KeyCode.X)) { animator.SetFloat("Blend", 10f); }
            else
                animator.SetFloat("Blend", 1f, 0.1f, Time.deltaTime);
        }
        if (Movez < 0f)
        {
            if (Input.GetKey(KeyCode.X)) { animator.SetFloat("Blend", 11f); }
            else
                animator.SetFloat("Blend", 6f);
        }
    }
    private void Run(float Movez)
    {
        moveSpeed = Runspeed;
        if (Movez > 0f)
            animator.SetFloat("Blend", 2f, 0.1f, Time.deltaTime);
        if (Movez < 0f)
            animator.SetFloat("Blend", 5f, 0.1f, Time.deltaTime);
    }
    private void Jump()
    {
    
        float jumpY = Mathf.Sqrt(JumpHeight * -2 * gravity);
        velocity.y = jumpY;
      
        
    }
}
