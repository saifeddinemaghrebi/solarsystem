using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamerController : MonoBehaviour
{
    //VARIABLES
    [SerializeField] private float RotationSensitivity;
    private Transform parent;
    // Start is called before the first frame update
    void Start()
    {
        parent = transform.parent;
    }

    // Update is called once per frame
    void Update()
    {
        Rotate();
    }
    private void Rotate()
    {
        float rotationy = Input.GetAxis("Horizontal")* RotationSensitivity * Time.deltaTime;
        parent.Rotate(Vector3.up,rotationy);
    }
}
